import 'package:flutter/material.dart';

double defaultMargin = 20.0;

Color primaryColor = const Color(0xff8E81E8);
Color secondaryColor = const Color(0xff38ABBE);
Color alertColor = const Color(0xffED6363);
Color priceColor = const Color(0xff2C96F1);
Color productPictureColor = const Color(0xffECEDEF);
Color subtitleColor = const Color(0xff504F5E);
Color backgrounColor1 = const Color(0xffF1F1F1);
Color backgrounColor2 = const Color(0xffF4F4F4);
Color backgroundColor3 = const Color(0xff92E3FD);
Color backgroundColor4 = const Color(0xffDDD2D2);
Color primaryTextColor = const Color(0xff2E2E2E);
Color secondaryTextColor = const Color(0xff6C5ECF);
Color thirdTextColor = const Color(0xffFF0F00);
Color buttonTextColor = const Color(0xffF1F0F2);
Color transparentColor = Colors.transparent;
Color arrowColor = const Color(0xff2E2E2E);

TextStyle primaryTextStyle =
    TextStyle(fontFamily: "Quicksand", color: primaryTextColor);

TextStyle secondaryTextStyle =
    TextStyle(fontFamily: "Quicksand", color: secondaryTextColor);

TextStyle thirdTextStyle =
    TextStyle(fontFamily: "Quicksand", color: thirdTextColor);

TextStyle priceTextStyle =
    TextStyle(fontFamily: "Quicksand", color: priceColor);

TextStyle buttonTextStyle =
    TextStyle(fontFamily: "Quicksand", color: buttonTextColor);

TextStyle subtitleTextStyle =
    TextStyle(fontFamily: "Quicksand", color: subtitleColor);

TextStyle purpleTextStyle =
    TextStyle(fontFamily: "Quicksand", color: primaryColor);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;
